# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import json

import rsxrpc
from typedef import *
from rsxrpc import *


import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.web


class RPCHandler(tornado.web.RequestHandler):

    def get(self):
        self.write('DO NOT SUPPORT HTTP GET')

    def post(self):
        # TODO : handle invalid RSX RPC call (json format error)
        rpcstr = self.get_argument('rpc', '')
        print 'POST data is %s' % (rpcstr)
        retRPC = rsxrpc.do_rpccall(rpcstr)
        self.write(json.dumps(retRPC))


def make_app():
    return tornado.web.Application([
        (r'/rpc', RPCHandler),
    ])
