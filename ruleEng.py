# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import json
import rsxrpc
import threading
import traceback
from typedef import *


def run_zone_status_change_notification(RSPResp, zonestatus, extendedstatus, zoneid, delay) :
    try:
        fp = open(CPF_TST_CFG, 'r')
        tst = json.load(fp)
        fp.close()

        for trigger in tst['TRIGGER']:
            print 'trigger = %s' % json.dumps(trigger)
            if trigger['event'] == 'IAS_ZONE_CLUSTER_ZoneStatusChangeNotification' \
                    and trigger['rule']['endpoint'] == RSPResp['srcep']\
                    and trigger['rule']['euinwk'] == RSPResp['euinwk']:
                strval = str(trigger['rule']['zoneStatus']).split(' ', 4)
                print('Rule=%s' % (strval))
                val = int(strval[1], 16)
                result = int(strval[3], 16)
                zonestatus = RSPResp['apppayload'][0] | RSPResp['apppayload'][1] << 8
                print 'val=%04X zonestatus=%04X result=%04X \n' % (val, zonestatus, result)
                if(val & zonestatus == result) :
                    for task in trigger['tasks'] :
                        func = getattr(rsxrpc, task['rpc_cmd'])
                        try:
                            runAfter = trigger['run_after']
                        except Exception :
                            runAfter = 0

                        if runAfter != 0 :
                            t = threading.Timer(runAfter, func, args=[task])
                            t.start()
                        else :
                            func(task)
    except :
        traceback.print_exc()
