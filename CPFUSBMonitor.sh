#!/bin/sh

#
# Thc script is used to monitor the CPFMAIN.py state.
# If the CPFMAIN is not running, re-launch it.
#
# Before launching CPFMAIN, call
# /usr/local/bin/cpf-notify -a -m action=plug,module=usb,moduleId=/dev/ttyUSB0,VID=10CE,PID=0D01
# to register the CPFMAIN
#
# When detect the CPFMAIN is not running, call
# /usr/local/bin/cpf-notify -a -m action=un-plug,module=usb,moduleId=/dev/ttyUSB0,VID=10CE,PID=0D01
# to un-register the CPFMAIN

while [ 1 ]
do

	if [ -z pgrep "CPFMAIN" ]; then
		/usr/local/bin/cpf-notify -a -m action=un-plug,module=usb,moduleId=/dev/ttyUSB0,VID=10CE,PID=0D01
		echo -n "Un-Plug Rosnoix Zigbee USB dongle"
	fi

	if [ -z pgrep 'CPFMAIN' ]; then
		/usr/bin/python /root/CPF/CPFMAIN.py &
		sleep 3
	fi

	if [ pgrep "CPFMAIN" ]; then
		/usr/local/bin/cpf-notify -a -m action=plug,module=usb,moduleId=/dev/ttyUSB0,VID=10CE,PID=0D01
		ehco -n "Plug Rosonix Zigbee USB dongle"
	fi

	sleep 60		

done