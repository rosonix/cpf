# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import select
import serial
import rspcallback as callbacks
from typedef import *
import traceback
import dbmgt

RSP_NODE_STATUS = 0xC0

HOSTNODE = {}

RSP_SERIAL_PORT = None


def printRSPRaw(RSPResp):
    print '\nRSPResp : %s' % (RSPResp['rawstr'])


def rsp_open_port(port):
    RSP_SERIAL_PORT = serial.Serial(port,
                                    baudrate     = 115200,
                                    bytesize     = serial.EIGHTBITS,
                                    parity       = serial.PARITY_NONE,
                                    stopbits     = serial.STOPBITS_ONE,
                                    rtscts       = 1,
                                    writeTimeout = .1,
                                    timeout      = .1 )
    return RSP_SERIAL_PORT


def rsp_serial_tick(rlist=[], wlist=[], xlist=[], timeout=0.1):
    rlist[0] = RSP_SERIAL_PORT
    rrlist, wwlist, xxlist = select.select(rlist, wlist, xlist, timeout)
    """ Handler the READ part """
    for l in rrlist:
        data = l.readline()
        if(len(data) > 0):
            rspstr                = data.decode('utf8')
            RSPResponseList       = rspstr.splitlines()[0].split(',')
            RSPResp               = {}
            RSPResp['rawstr']     = rspstr
            RSPResp['type']       = RSPResponseList[0]
            RSPResp['sequence']   = int(RSPResponseList[1], 16)
            RSPResp['euinwk']     = RSPResponseList[2]
            RSPResp['rssi']       = int(RSPResponseList[3], 16)
            RSPResp['lqi']        = int(RSPResponseList[4], 16)
            RSPResp['profileid']  = int(RSPResponseList[5], 16)
            RSPResp['clusterid']  = int(RSPResponseList[6], 16)
            RSPResp['srcep']      = int(RSPResponseList[7], 16)
            RSPResp['destep']     = int(RSPResponseList[8], 16)
            RSPResp['apsopt']     = int(RSPResponseList[9], 16)
            RSPResp['cmdid']      = int(RSPResponseList[10], 16)
            RSPResp['frametype']  = int(RSPResponseList[11], 16)
            RSPResp['direction']  = int(RSPResponseList[12], 16)
            RSPResp['mfgid']      = int(RSPResponseList[13], 16)
            RSPResp['apppayload'] = bytearray.fromhex(RSPResponseList[14])
            dbmgt.add_rsp_record(RSPResp)
            return RSPResp


def rsp_command(cmd):
    assert(RSP_SERIAL_PORT.isOpen())
    print 'RSPcmd=%s' % (cmd)
    cmd += '\r\n'
    RSP_SERIAL_PORT.write(cmd.encode('utf8'))


def dispatch_command(RSPResp):
    assert(RSPResp)
    # todo:DO checking the checksum of the RSPResp
    try:
        printRSPRaw(RSPResp)
        if(RSPResp['type'] == '$10'):
            if(RSPResp['profileid'] == RSX_PROFILE_ID and RSPResp['clusterid'] == RSX_CLUSTER_ID):
                if(RSPResp['cmdid'] == RSP_NODE_STATUS):
                    HOSTNODE['euinwk']        = RSPResp['euinwk']
                    HOSTNODE['networkstatus'] = RSPResp['frametype']
                    HOSTNODE['panid']         = RSPResp['mfgid']
                    HOSTNODE['channel']       = RSPResp['direction']
                    print 'HOST : %s:%d:0x%X:%d \n' % (
                           HOSTNODE['euinwk'],
                           HOSTNODE['networkstatus'],
                           HOSTNODE['panid'],
                           HOSTNODE['channel'] )

                callbacks.rsp_status_message_callback(RSPResp)

        elif(RSPResp['profileid'] != ZDP_PROFILE_ID and RSPResp['type'] == '$80'):
                callbacks.ZCL_CALLBACKS[RSPResp['frametype']][RSPResp['cmdid']](RSPResp)

        elif(RSPResp['profileid'] == ZDP_PROFILE_ID and RSPResp['type'] == '$80'):
                callbacks.ZDP_CALLBACKS[RSPResp['clusterid']](RSPResp)

        elif(RSPResp['profileid'] == RSX_PROFILE_ID and RSPResp['type'] == '$21'):
                callbacks.rsp_trustcenter_report_callback(RSPResp)

        else:
            print 'UNHANDLE RSP %s ' % (RSPResp['rawstr'])

    except Exception as e:
        traceback.print_exc()
        print("UNHANDLE Zigbee Message %s \n" % RSPResp['rawstr'])
        raise e
