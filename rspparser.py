# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

from typedef import *
import traceback

'''
    return the read attribute response as dict
    dict[attrid] ={
        'status' : ...
        'dtype'  : ...
        'value'  : ...
    }
'''


def parse_zcl_read_attribute_response(RSPResp):
    ret = {}
    appdata = RSPResp['apppayload']
    index = 0
    while index < len(appdata) :
        print 'size=' + str(len(appdata)) + ' index=' + str(index)
        attrid = appdata[index + 1] << 8 | appdata[index]
        ret[attrid] = {}
        index += 2
        status = appdata[index]
        ret[attrid] = {'status': status}
        index += 1
        print 'status=0x%02X attrid=0x%04X ' % (ret[attrid]['status'], attrid)
        if ret[attrid]['status'] == 0x00 :
            dtype = appdata[index]
            index += 1
            ret[attrid]['dtype'] = dtype
            if ret[attrid]['dtype'] >= ZCL_OCTET_STRING_ATTRIBUTE_TYPE :
                size = appdata[index]
                index += 1
                value = appdata[index:index + size + 1]
                index += size
                ret[attrid]['value'] = str(value)
                print 'value = %s' % (ret[attrid]['value'])
            else:
                try:
                    dsize = ZCLDataSize[(dtype,)] # The type of keys in ZCLDataSize is tuple, not string
                    value = 0
                    for i in range(0, dsize):
                        value = value | appdata[index] << (i * 8)
                        index += 1

                    ret[attrid]['value'] = value
                    print('value = %d' % (value))
                except Exception:
                    traceback.print_exc()

    return ret


def parse_zcl_report_attribute(RSPResp) :
    ret = {}
    appdata = RSPResp['apppayload']
    index = 0
    while index < len(appdata) :
        print 'size=' + str(len(appdata)) + ' index=' + str(index)
        attrid = appdata[index + 1] << 8 | appdata[index]
        ret[attrid] = {}
        index += 2
        dtype = appdata[index]
        ret[attrid] = {'dtype': dtype}
        index += 1
        print 'attrid=0x%04X dtype=0x%02X ' % (attrid, ret[attrid]['dtype'])
        if ret[attrid]['dtype'] >= ZCL_OCTET_STRING_ATTRIBUTE_TYPE :
            size = appdata[index]
            index += 1
            value = appdata[index:index + size + 1]
            index += size
            ret[attrid]['value'] = str(value)
            print 'value = %s' % (ret[attrid]['value'])
        else:
            try:
                dsize = ZCLDataSize[(dtype,)] # The type of keys in ZCLDataSize is tuple, not string
                value = 0
                for i in range(0, dsize):
                    value = value | appdata[index] << (i * 8)
                    index += 1

                ret[attrid]['value'] = value
                print('value = %d' % (value))
            except Exception:
                traceback.print_exc()

    return ret


'''
Return the ZDP Simple Descriptor Response as dict
info = {'nwkaddr':nodeid,
                'endpoint':endpoint,
                'profileId':profileId,
                'inputclusters':inputcluster,
                'outputclusters':outputcluster,
                'deviceid':deviceid,
                'deviceversion':deviceversion,
                'status':status }
'''


def parse_rsp_zdp_simple_descriptor_response(RSPResp):
    appdata = RSPResp['apppayload']
    inputcluster = []
    outputcluster = []
    index = 0
    status = appdata[index]
    index += 1
    if(status == 0x00):
        nodeid = appdata[index] | appdata[index + 1] << 8
        index += 2
        length = appdata[index]
        index += 1
        endpoint = appdata[index]
        index += 1
        profileId = appdata[index] | appdata[index + 1] << 8
        index += 2
        deviceid = appdata[index] | appdata[index + 1] << 8
        index += 2
        deviceversion = appdata[index]
        index += 1
        count = appdata[index]
        index += 1
        for i in range(0, count):
            cluster = appdata[index] | appdata[index + 1] << 8
            inputcluster.append(cluster)
            index += 2
        count = appdata[index]
        index += 1
        for i in range(0, count):
            cluster = appdata[index] | appdata[index + 1] << 8
            outputcluster.append(cluster)
            index += 2

        info = {'nwkaddr': nodeid,
                'endpoint': endpoint,
                'profileId': profileId,
                'inputclusters': inputcluster,
                'outputclusters': outputcluster,
                'deviceid': deviceid,
                'deviceversion': deviceversion,
                'status': status}

        return info
