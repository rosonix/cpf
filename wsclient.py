# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import json
import threading
import types
import os

import rsxrpc
from typedef import *
from rsxrpc import *
import websocket


def on_message(ws, message):
    print 'on_message message=%s \n' % (message)
    retRPC = rsxrpc.do_rpccall(message)
    if (isinstance(retRPC, types.StringType) and (retRPC == 'api_init')):
        ws.wsclient.apiinit['token'] = str(rsxrpc.TOKEN)
        print 'api_init TOKEN is %s ' % (ws.wsclient.apiinit['token'])
    else:
        ws.send(json.dumps(retRPC))


def on_close(ws):
    print "Remote CLOSE , System exit"
    os._exit(-1)


def on_open(ws):
    rpccmd = api_init_request(ws.wsclient.apiinit['username'],
                            ws.wsclient.apiinit['password'],
                            ws.wsclient.apiinit['gwid'],
                            ws.wsclient.apiinit['gwname'])
    ws.send(json.dumps(rpccmd))


class WSClient :

    def __init__(self, serverurl, username, password, gwid, gwname, verbose=False) :

        self.onOpen    = on_open
        self.onClose   = on_close
        self.onMessage = on_message
        self.apiinit   = {}
        self.ws = websocket.WebSocketApp(serverurl ,
                                        on_open    = self.onOpen ,
                                        on_message = self.onMessage ,
                                        on_close   = self.onClose)
        self.apiinit['username'] = username
        self.apiinit['password'] = password
        self.apiinit['gwid']     = gwid
        self.apiinit['gwname']   = gwname
        self.apiinit['token']    = 'notoken'
        self.ws.wsclient = self
        self.wst = threading.Thread(
                                    target  = self.ws.run_forever,
                                    verbose = verbose,
                                    name    = 'ECTWebSocketClient',
                                    kwargs  = {'ping_interval': 30}
                                )

        self.wst.start()
        websocket.enableTrace(False)

    def send(self, message) :
        rpccmd = json.loads(message)
        rpccmd['token'] = self.apiinit['token']
        msg = json.dumps(rpccmd, indent=None)
        print 'send message : %s' % (msg)
        self.ws.send(msg)
