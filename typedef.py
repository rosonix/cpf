# -*- coding: utf-8 -*-
# v1.0.10
# add a new RPC call device_AllowToJoin
# process WebSocketClosedException. make program do sys.exit(-1)
# websocket server password will algorithm is MD5 hash of username

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

VERSION = 'RSP Python Framework v1.0.11'

STORE = {}

WEBSERVER_PORT        = 8080

THE_SERIAL_PORT       = '/dev/ttyUSB0'

CPF_DEVICE_DB         = '/tmp/CPF.DEVICE.DB'
CPF_SCHEDULE_CFG      = '/tmp/CPF.schedule.cfg'
CPF_TST_CFG           = '/tmp/CPF.tst.cfg'

CPF_CRON_TABLE        = '/etc/crontab'

CPF_HOME              = '/root/CPF/'


'''
example of /etc/cpf/cpfacp.dat
{
    "coswe.weboscket.url":"ws://socket2.ecoluminatech.com:8999",
    "coswe.username":"coswedemo@gmail.com",
    "coswe.password":MD5("coswedemo@gmail.com")
}

'''
CPF_SYSTEM_CFG       = '/etc/cpf/cpfacp2.dat'

'''
WEBSOCKET_SERVER_URL  = 'ws://socket2.ecoluminatech.com:8999'
WEBSOCKET_USERNAME    = 'coswedemo@gmail.com'
WEBSOCKET_PASSWORD    = 'coswedemo1234'
'''


# ZCL HEADER
ZCL_FRAMETYPE_PROFILE_WIDE     = 0x00
ZCL_FRAMETYPE_CLUSTER_SPECIFIC = 0x01
ZCL_DIRECTION_CLIENT_TO_SERVER = 0x00
ZCL_DIRECTION_SERVER_TO_CLIENT = 0x01


# ZCL Type
ZCL_OCTET_STRING_ATTRIBUTE_TYPE      = 0x41
ZCL_CHAR_STRING_ATTRIBUTE_TYPE       = 0x42
ZCL_LONG_OCTET_STRING_ATTRIBUTE_TYPE = 0x43
ZCL_LONG_CHAR_STRING_ATTRIBUTE_TYPE  = 0x44

ZCL_DATA8_ATTRIBUTE_TYPE                          = 0x08, # 8-bit data
ZCL_DATA16_ATTRIBUTE_TYPE                         = 0x09, # 16-bit data
ZCL_DATA32_ATTRIBUTE_TYPE                         = 0x0B, # 32-bit data
ZCL_DATA64_ATTRIBUTE_TYPE                         = 0x0F, # 64-bit data
ZCL_BOOLEAN_ATTRIBUTE_TYPE                        = 0x10, # Boolean
ZCL_BITMAP8_ATTRIBUTE_TYPE                        = 0x18, # 8-bit bitmap
ZCL_BITMAP16_ATTRIBUTE_TYPE                       = 0x19, # 16-bit bitmap
ZCL_BITMAP32_ATTRIBUTE_TYPE                       = 0x1B, # 32-bit bitmap
ZCL_BITMAP64_ATTRIBUTE_TYPE                       = 0x1F, # 64-bit bitmap
ZCL_INT8U_ATTRIBUTE_TYPE                          = 0x20, # Unsigned 8-bit integer
ZCL_INT16U_ATTRIBUTE_TYPE                         = 0x21, # Unsigned 16-bit integer
ZCL_INT32U_ATTRIBUTE_TYPE                         = 0x23, # Unsigned 32-bit integer
ZCL_INT64U_ATTRIBUTE_TYPE                         = 0x27, # Unsigned 64-bit integer
ZCL_INT8S_ATTRIBUTE_TYPE                          = 0x28, # Signed 8-bit integer
ZCL_INT16S_ATTRIBUTE_TYPE                         = 0x29, # Signed 16-bit integer
ZCL_INT32S_ATTRIBUTE_TYPE                         = 0x2B, # Signed 32-bit integer
ZCL_INT64S_ATTRIBUTE_TYPE                         = 0x2F, # Signed 64-bit integer
ZCL_ENUM8_ATTRIBUTE_TYPE                          = 0x30, # 8-bit enumeration
ZCL_ENUM16_ATTRIBUTE_TYPE                         = 0x31, # 16-bit enumeration

# ZCL DATA SIZE
ZCLDataSize = {
    ZCL_DATA8_ATTRIBUTE_TYPE    : 1,
    ZCL_DATA16_ATTRIBUTE_TYPE   : 2,
    ZCL_DATA32_ATTRIBUTE_TYPE   : 4,
    ZCL_DATA64_ATTRIBUTE_TYPE   : 8,
    ZCL_BOOLEAN_ATTRIBUTE_TYPE  : 1,
    ZCL_BITMAP8_ATTRIBUTE_TYPE  : 1,
    ZCL_BITMAP16_ATTRIBUTE_TYPE : 2,
    ZCL_BITMAP32_ATTRIBUTE_TYPE : 4,
    ZCL_BITMAP64_ATTRIBUTE_TYPE : 8,
    ZCL_INT8U_ATTRIBUTE_TYPE    : 1,
    ZCL_INT16U_ATTRIBUTE_TYPE   : 2,
    ZCL_INT32U_ATTRIBUTE_TYPE   : 4,
    ZCL_INT64U_ATTRIBUTE_TYPE   : 8,
    ZCL_INT8S_ATTRIBUTE_TYPE    : 1,
    ZCL_INT16S_ATTRIBUTE_TYPE   : 2,
    ZCL_INT32S_ATTRIBUTE_TYPE   : 4,
    ZCL_INT64S_ATTRIBUTE_TYPE   : 8,
    ZCL_ENUM8_ATTRIBUTE_TYPE    : 1,
    ZCL_ENUM16_ATTRIBUTE_TYPE   : 2,
}


# ZCL Profile wide command
ZCL_READ_ATTRIBUTE_RESPONSE   = 0x01
ZCL_WRITE_ATTRIBUTE_RESPONSE  = 0X02
ZCL_REPORT_ATTRIBUTE_RESPONSE = 0x0A
ZCL_DEFAULT_RESPONSE_COMMAND  = 0x0B

# ZCL Cluster specific command
ZCL_ALARM_CLUSTER_Alarm = 0x00
ZCL_IAS_ZONE_CLUSTER_ZoneStatusChangeNotification = 0x00
ZCL_IAS_ZONE_CLUSTER_ZoneEnrollRequest            = 0x01


# ZCL Profile
ZDP_PROFILE_ID = 0x0000
HA_PROFILE_ID  = 0x0104
RSX_PROFILE_ID = 0xC001


# ZCL Cluster
RSX_CLUSTER_ID          = 0x0C00
ZCL_BASIC_CLUSTER_ID    = 0x0000
ZCL_IDENTIFY_CLUSTER_ID = 0x0003
ZCL_ONOFF_CLUSTER_ID    = 0x0006
ZCL_ALARM_CLUSTER_ID    = 0x0009
ZCL_IAS_ZONE_CLUSTER_ID = 0x0500
ZCL_IAS_ACE_CLUSTER_ID  = 0x0501
ZCL_IAS_WD_CLUSTER_ID   = 0x0502


# ZDP
ZDP_NETWORK_ADDRESS_RESPONSE   = 0x8000
ZDP_IEEE_ADDRESS_RESPONSE      = 0x8001
ZDP_SIMPLE_DESCRIPTOR_RESPONSE = 0x8004
ZDP_ACTIVE_ENDPOINTS_RESPONSE  = 0x8005
ZDP_MATCH_DESCRIPTORS_RESPONSE = 0x8006
ZDP_DEVICE_ANNOUNCE_RESPONSE   = 0x0013


# RSP Command
RSPCMD_GET_HOST_NODE = '$00,01,FFFFFFFFFFFF0000,C001,0C00,FF,FF,FFFF,C0,FF,FF,FFFF'
RSPCMD_MATCH_DESC_REQ = '$00,02,FFFFFFFFFFFFFFFD,0000,0006,00,00,FFFF,FF,FF,FF,FFFF,04010000'


# CREATE TABLE
CREATE_RSP_RECORD_TABLE = (
    "CREATE TABLE rsp_record" +
    "( rectime INTEGER," +
    "type INTEGER," +
    "sequence INTEGER," +
    "euinwk TEXT," +
    "rssi INTEGER," +
    "lqi INTEGER," +
    "profileid INTEGER," +
    "clusterid INTEGER," +
    "srcep INTEGER," +
    "destep INTEGER," +
    "cmdid INTEGER," +
    "frametype INTEGER," +
    "direction INTEGER," +
    "mfgid INTEGER," +
    "apppayload TEXT)")

INSERT_RSP_RECORD = (
    " INSERT INTO rsp_record VALUES " +
    " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
)

