# README #

### What is this repository for? ###
The Rosonix Zigbee framework for CPF. The Rosonix Zigbee Framework is developed by Rosonix. It is using RSP(Rosonix Serial Protocol) to get message from zigbee device or send message to Zigbee device. The Rosonix Zigbee Framework is develop by C or Python. The CPF will use the Python based framework.

### How do I get set up? ###

* Summary of set up
```
Python package:
pip install requests
pip install tornado
pip install websocket-client

WebCam package
yum install xawtv

CPF SDK installation:
1. Download cpf-device-development-package-env-20160203.tar.xz
   https://drive.google.com/file/d/0B8jK4lzKxt46eE1FMTJVakVBYWM/view?usp=sharing
2. create one user in linpus lite
3. copy cpf-device-development-package-env-20160203.tar.xz to cpf device
4. run command in console
    - tar -xf cpf-device-development-package-env-20160203.tar.xz
    - cd env
    - sudo su
    - for i in `ls *.rpm`;do rpm -i $i --force --nodeps;done
    - ln -s /usr/lib64/libmicrohttpd.so.10 /usr/lib64/libmicrohttpd.so
    - exit
```

 
* Web Server Configuration
```
The Web Server can accept the RSX RPC call.  
HTTP Port : 8080

example of cURL:
echo 'rpc=$RSX_RPC_CALL' | curl -d @- http://$CPF_IP:8080/rpc
```

* Database configuration
```
The CPF will create a small database by SQLIte. All the zigbee incoming message (type=$8x and $10 aps ack) will be stored in the database.

The other one is device database. It is stored in /tmp/CPF.DEVICE.DB in json format.
```

* How to run
```
run /root/CPF/CPFMAIN.py
```

* SCHEDULE
```
Using with CPF crond
```

* ZONE Notification
```
/tmp/CPF.tst.cfg
```