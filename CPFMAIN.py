#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

from typedef import *
from rsxrpc import *

import os
import traceback
import thread
import time
import rsp
import devicemgt
import dbmgt
import hashlib

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.web
import serial
import websocket
import wsclient
import wsserver

webloop = None

rspPCB = None


# The serial_tick is running in the thread
# the sys.exit is the same as thread.exit()
def serial_tick():
    try:
        resp = rsp.rsp_serial_tick(rlist=[rsp.RSP_SERIAL_PORT])
        if resp is not None:
            rsp.dispatch_command(resp)
    except websocket.WebSocketConnectionClosedException:
        print("WebSocketConnectionClosedException\n")
        traceback.print_exc()
        os._exit(1)
    except serial.SerialException:
        print("SerialException\n")
        traceback.print_exc()
        os._exit(1)
    except Exception:
        traceback.print_exc()


def main(**argv):

    dbmgt.init_data_store('/root/rsp.db')

    rsp.RSP_SERIAL_PORT = rsp.rsp_open_port(THE_SERIAL_PORT)

    if rsp.RSP_SERIAL_PORT.isOpen() is False:
        print 'The %s OPEN failed' % (THE_SERIAL_PORT)
        exit()

    while not ('euinwk' in rsp.HOSTNODE):
        rsp.rsp_command(RSPCMD_GET_HOST_NODE)
        serial_tick()
        time.sleep(0.5)

    devicemgt.load()

    print 'Device Discovery'
    devicemgt.discover_ha_devices()

    gwname = 'iamgateway' + '_' + str(rsp.HOSTNODE['euinwk']) + '_' + str(rsp.HOSTNODE['panid']) + '_' + str(rsp.HOSTNODE['channel'])

# The USER id will come from /etc/cpf/cpfacp.dat
    '''
    tmp1fp = open('/etc/cpf/cpfacp.dat', 'r')
    tmp1 = json.load(tmp1fp)
    accUserId = tmp1['userId']
    accUserPassword = hashlib.md5(accUserId).hexdigest()
    accFP = open(CPF_SYSTEM_CFG, 'r')
    accCFG = json.load(accFP)
    accFP.close()
    tmp1fp.close()
    accCFG['coswe.username'] = accUserId
    accCFG['coswe.password'] = accUserPassword

    '''
    accCFG = {}
    accCFG['coswe.weboscket.url'] = 'ws://socket.cpf.ecoluminatech.com:9001'
    accCFG['coswe.username'] = '13365800'
    accCFG['coswe.password'] = '3caf250a980f5b8ef72058e305a323eb'

    print 'userid %s userpasswd %s \n' % (accCFG['coswe.username'], accCFG['coswe.password'])
    STORE['wsclient'] = wsclient.WSClient(accCFG['coswe.weboscket.url'],
                                          accCFG['coswe.username'],
                                          accCFG['coswe.password'],
                                          rsp.HOSTNODE['euinwk'],
                                          gwname)

    app = wsserver.make_app()
    app.listen(WEBSERVER_PORT)

    webloop = tornado.ioloop.IOLoop.current()
    rspPCB = tornado.ioloop.PeriodicCallback(callback=serial_tick, callback_time=50)

    thread.start_new_thread(rsxrpc.report_device_list_threadfun, (30,))

    rspPCB.start()
    webloop.start()


if __name__ == "__main__":
    try:
        main()
    except Exception:
        traceback.print_exc()
        os._exit(-1)
    except KeyboardInterrupt:
        traceback.print_exc()
        os._exit(-1)
