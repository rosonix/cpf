# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import sys
import json
import traceback
import requests
from typedef import *


def main(schid) :
    try:
        print 'CRONTAB schid = %s' % (schid)
        fp = open(CPF_SCHEDULE_CFG, 'r')
        sceneTable = json.load(fp)
        idx = 0
        for job in sceneTable['SCHEDULE'] :
            if(job['id'] == schid) :
                for task in sceneTable['SCHEDULE'][idx]['tasks'] :
                    postData = {'rpc': json.dumps(task)}
                    response = requests.post('http://127.0.0.1:%d/rpc' % (WEBSERVER_PORT), data=postData )
                    print 'Response : %s' % (response.text)

            idx += 1
        fp.close()
    except Exception :
        traceback.print_exc()


if __name__ == '__main__' :
    main(sys.argv[1])
