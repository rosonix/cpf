# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''

import rsp
import json
from typedef import *
import traceback

'''
There are device information existing in the Device object
euinwk          : The eui64 address or network address using RSP address format , string
endpoint        : The endpoint the device hosts, number
nwkaddr         : The 2 bytes network address
deviceid        : The 2 bytes device id
deviceversion   : The 1 byte device version
inputclusters   : The input clusters the devices hosts, list of number
outputclusters  : The output clusters the devices hosts, lust of number
modelidentifier : The firmware information of the device, string
zoneid          : The ZONE id of the device
zonetype        : The ZONE type of the device
mfgcode         : The Manufacturer code
profileId       : The Zigbee Profile Id

'''

DEVICES = []


def add_device(device):
    remove_device(device)
    DEVICES.append(device)


def device_leave(device) :
    for dev in DEVICES[:]:
        if (dev.euinwk == device.euinwk):
            remove_device(dev)


def remove_device(device):
    for dev in DEVICES:
        if (dev.euinwk == device.euinwk and dev.endpoint == device.endpoint) or \
           (dev.euinwk == device.euinwk and dev.endpoint == 0xFF):
            print 'remove_device euinwk=%s endpoint=%d ' % (dev.euinwk, dev.endpoint)
            DEVICES.remove(dev)


def get_device(euinwk, endpoint):
    for dev in DEVICES:
        if (dev.euinwk == euinwk and dev.endpoint == endpoint):
            return dev


def get_devices():
    return DEVICES


def print_devices():
    print'\nIn DeviceManagement :'
    for dev in DEVICES:
        print dev
    print('\n\n')


def discover_ha_devices():
    rsp.rsp_command(RSPCMD_MATCH_DESC_REQ)


def zdp_simple_descriptor_request(euinwk, endpoint):
    cmd = '$00,01,' + euinwk + ',0000,0004,00,00,FFFF,FF,FF' + ',%02X' % (endpoint) + ',FFFF'
    rsp.rsp_command(cmd)


def serialize():
    fp = open(CPF_DEVICE_DB, 'w')
    root = {
        'DEVICES': []
    }
    devices = root['DEVICES']

    for dev in DEVICES :
        d = {}
        for key in dev.__dict__ :
            d[key] = dev.__dict__[key]
            # print 'key:val =>' + key + ' : ' + str(d[key])
        devices.append(d)

    json.dump(root, fp)
    fp.close()


def load() :
    try :
        fp = open(CPF_DEVICE_DB, 'r')
        root = json.load(fp)
        fp.close()
        devices = root['DEVICES']
        for d in devices :
            dev = Device(d['euinwk'])
            dev.update_device(**d)
            DEVICES.append(dev)

        print_devices()
    except :
        traceback.print_exc()


class Device:

    def __init__(self,
                 euinwk,
                 endpoint=0xFF,
                 nwkaddr=0xFFFF,
                 deviceid=0xFFFF,
                 deviceversion=0xFF,
                 inputclusters=[],
                 outputclusters=[],
                 modelidentifier='unknown',
                 zoneid=0xFF,
                 zonetype=0xFFFF,
                 mfgcode=0xFFFF,
                 profileId=0xFFFF):
        self.euinwk          = euinwk
        self.endpoint        = endpoint
        self.nwkaddr         = nwkaddr
        self.deviceid        = deviceid
        self.deviceversion   = deviceversion
        self.inputclusters   = inputclusters
        self.outputclusters  = outputclusters
        self.modelidentifier = modelidentifier
        self.zoneid          = zoneid
        self.zonetype        = zonetype
        self.mfgcode         = mfgcode
        self.profileId       = profileId

    def update_device(self, **kwargs):
        for key in kwargs:
            if(key == 'euinwk'):
                self.euinwk = kwargs[key]
            elif(key == 'endpoint'):
                self.endpoint = kwargs[key]
            elif(key == 'nwkaddr'):
                self.nwkaddr = kwargs[key]
            elif(key == 'inputclusters'):
                self.inputclusters = kwargs[key]
            elif(key == 'outputclusters'):
                self.outputclusters = kwargs[key]
            elif(key == 'modelidentifier'):
                self.modelidentifier = kwargs[key]
            elif(key == 'zonid'):
                self.zoneid = kwargs[key]
            elif(key == 'zonetype'):
                self.zonetype = kwargs[key]
            elif(key == 'mfgcode'):
                self.mfgcode = kwargs[key]
            elif(key == 'profileId'):
                self.profileId = kwargs[key]

    def zdp_simple_descriptor_request(self):
        cmd = '$00,01,' + self.euinwk + ',0000,0004,00,00,FFFF,FF,FF' + ',%02X' % (self.endpoint) + ',FFFF'
        rsp.rsp_command(cmd)

    def zdp_active_endpoint_request(self, profileid=0x0104, clusterid=0x0000):
        cmd = '$00,01,' + self.euinwk + ',0000,0005,00,00,FFFF,FF,FF,FF,FFFF'
        rsp.rsp_command(cmd)

    def __str__(self):
        x = ['Device : ']
        x.append(json.dumps(self.__dict__))
        return ''.join(x)
