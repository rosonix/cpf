# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com

If The Framework support new RSX callback, register it to
ZCL_PROFILE_WIDE_COMMANDS or
ZCL_CLUSTER_SPECIFIC_COMMANDS or
ZDP_CALLBACKS


'''

import sys
import thread
import devicemgt
import json
import struct
import time
import rspparser as parser
from typedef import *
from ruleEng import *


def rsp_status_message_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)


def rsp_zcl_read_attribute_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    val = parser.parse_zcl_read_attribute_response(RSPResp)
    print 'val=%s' % (json.dumps(val))

    for attrid in val:
        if RSPResp['clusterid'] == ZCL_BASIC_CLUSTER_ID and attrid == 0x0005:
            device = devicemgt.get_device(RSPResp['euinwk'], RSPResp['srcep'])
            if device is not None:
                info = {'modelidentifier': str(val[attrid]['value'])}
                device.update_device(**info)
                devicemgt.serialize()

        elif RSPResp['clusterid'] == ZCL_ONOFF_CLUSTER_ID and attrid == 0x0000:
            RPCCmd = {
                rpc_cmd: 'list_onoff_switch_status',
                rpc_id: 1,
                rpc_return: {
                    device_list: [
                        {
                            'euinwk'      : RSPResp['euinwk'],
                            'endpoint'    : RSPResp['srcep'],
                            'clusterid'   : RSPResp['clusterid'],
                            'attributeid' : attrid,
                            'value'       : val[attrid]['value'],
                            'rectime'     : int(time.time())
                        }
                    ]
                }
            }
            wc = STORE['wsclient']
            wc.send(json.dumps(RPCCmd))


def rsp_zcl_write_attribute_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)


def rsp_zcl_default_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)


def rsp_zcl_report_attribute_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    val = parser.parse_zcl_report_attribute(RSPResp)
    for attrid in val :
        if RSPResp['clusterid'] == ZCL_ONOFF_CLUSTER_ID and attrid == 0x0000 :
            msg = {
                'rpc_cmd'    : 'list_onoff_switch_status',
                'rpc_err'    : {'err': 0},
                'rpc_return' :
                {
                    'device_list' :
                    [
                        {
                            'euinwk'      : RSPResp['euinwk'],
                            'endpoint'    : RSPResp['srcep'],
                            'clusterid'   : RSPResp['clusterid'],
                            'attributeid' : attrid,
                            'value'       : val[attrid]['value'],
                            'rectime'     : int(time.time())
                        }
                    ]
                }
            }
        STORE['wsclient'].send(json.dumps(msg))


def rsp_zcl_ALARM_CLUSTER_Alarm_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)


def rsp_zcl_IAS_ZONE_CLUSTER_ZoneStatusChangeNotification_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    appdata = RSPResp['apppayload']
    zonestatus, extendedstatus, zoneid, delay = struct.unpack('<H2BH', appdata)
    print 'zonestatus=%04X extendedstatus=%02X zoneid=%02X delay=%04X' % (zonestatus, extendedstatus, zoneid, delay)
    run_zone_status_change_notification(RSPResp, zonestatus, extendedstatus, zoneid, delay)

    alarmcluster = 0
    alarmcode    = 0

    if RSPResp['srcep'] == 0x01 :
        alarmcluster = 0x0406
        alarmcode    = 0x01

    elif RSPResp['srcep'] == 0x02 :
        alarmcluster = 0x000F
        alarmcode    = zonestatus & 0x0001

    ectalarm = {
        'rpc_cmd' : 'ect_device_alarm',
        'rpc_id' : RSPResp['sequence'],
        'rpc_param': {
            'euinwk' : RSPResp['euinwk'],
            'endpoint' : RSPResp['srcep'],
            'alarm_clusterid' : alarmcluster,
            'alarm_code' : alarmcode
        }
    }

    STORE['wsclient'].send(json.dumps(ectalarm))


def rsp_zcl_IAS_ZONE_CLUSTER_ZoneEnrollRequest_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    appdata  = RSPResp['apppayload']
    index    = 0
    zonetype = appdata[index] | appdata[index + 1] << 8
    index    += 2
    mfgcode  = appdata[index] | appdata[index + 1] << 8
    device   = devicemgt.get_device(RSPResp['euinwk'], RSPResp['srcep'])
    if(device is not None):
        info = {'zonetype': zonetype, 'mfgcode': mfgcode}
        device.update_device(**info)

    devicemgt.serialize()


def rsp_zdp_address_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)


def rsp_zdp_simple_descriptor_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    info = parser.parse_rsp_zdp_simple_descriptor_response(RSPResp)

    device = devicemgt.get_device(RSPResp['euinwk'], info['endpoint'])

    if device is None:
        device = devicemgt.Device(RSPResp['euinwk'])
        devicemgt.add_device(device)

    device.update_device(**info)
    devicemgt.serialize()
    devicemgt.print_devices()


def rsp_zdp_active_endpoints_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    appdata   = RSPResp['apppayload']
    index     = 0
    ZDPStatus = appdata[index]
    index     += 1
    if(ZDPStatus == 0x00):
        nwkaddr = appdata[index] | appdata[index + 1] << 8
        index   += 2
        count   = appdata[index]
        index   += 1
        i       = 0
        while (i < count):
            endpoint = appdata[index + i]
            device   = devicemgt.get_device(RSPResp['euinwk'], endpoint)
            info = {'euinwk': RSPResp['euinwk'], 'endpoint': endpoint , 'nwkaddr': nwkaddr}

            if(device is None):
                device = devicemgt.Device(RSPResp['euinwk'])
                devicemgt.add_device(device)

            device.update_device(**info)
            devicemgt.serialize()
            device.zdp_simple_descriptor_request()
            i += 1


def rsp_zdp_match_descriptors_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    appdata   = RSPResp['apppayload']
    index     = 0
    ZDPStatus = appdata[index]
    index     += 1
    if ZDPStatus == 0x00 :
        nwkaddr = appdata[index] | appdata[index + 1] << 8
        index   += 2
        count   = appdata[index]
        index   += 1
        i       = 0
        while i < count:
            endpoint = appdata[index + i]
            devicemgt.zdp_simple_descriptor_request('FFFFFFFFFFFF%04X' % (nwkaddr), endpoint)
            i += 1


def rsp_zdp_device_announce_response_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    thread.start_new_thread(rsxrpc.report_device_list_threadfun, (60,))
    device = devicemgt.get_device(RSPResp['euinwk'], 0xFF)
    if(device is None):
        device = devicemgt.Device(RSPResp['euinwk'])
        devicemgt.add_device(device)
        devicemgt.serialize()
        device.zdp_active_endpoint_request()


def rsp_trustcenter_report_callback(RSPResp):
    print '%s' % (sys._getframe().f_code.co_name)
    appdata = RSPResp['apppayload']

    if(appdata[0] == 0x02) :
        '''Device Zigbee NWK Leave network command receive'''
        print '%s will be removed' % (RSPResp['euinwk'])
        dev = devicemgt.Device(RSPResp['euinwk'])
        devicemgt.device_leave(dev)

    devicemgt.serialize()
    devicemgt.print_devices()


'''
######################################################################################
#
#  CALLBACKS Register
#  Any new new implemented callback should be register here.
#
######################################################################################
'''

ZCL_PROFILE_WIDE_COMMANDS = {
    ZCL_READ_ATTRIBUTE_RESPONSE   : rsp_zcl_read_attribute_response_callback,
    ZCL_WRITE_ATTRIBUTE_RESPONSE  : rsp_zcl_write_attribute_response_callback,
    ZCL_REPORT_ATTRIBUTE_RESPONSE : rsp_zcl_report_attribute_callback,
    ZCL_DEFAULT_RESPONSE_COMMAND  : rsp_zcl_default_response_callback,
}


ZCL_CLUSTER_SPECIFIC_COMMANDS = {
    ZCL_ALARM_CLUSTER_Alarm                           : rsp_zcl_ALARM_CLUSTER_Alarm_callback,
    ZCL_IAS_ZONE_CLUSTER_ZoneStatusChangeNotification : rsp_zcl_IAS_ZONE_CLUSTER_ZoneStatusChangeNotification_callback,
    ZCL_IAS_ZONE_CLUSTER_ZoneEnrollRequest            : rsp_zcl_IAS_ZONE_CLUSTER_ZoneEnrollRequest_callback,
}


ZDP_CALLBACKS = {
    ZDP_NETWORK_ADDRESS_RESPONSE   : rsp_zdp_address_response_callback,
    ZDP_IEEE_ADDRESS_RESPONSE      : rsp_zdp_address_response_callback,
    ZDP_SIMPLE_DESCRIPTOR_RESPONSE : rsp_zdp_simple_descriptor_response_callback,
    ZDP_ACTIVE_ENDPOINTS_RESPONSE  : rsp_zdp_active_endpoints_response_callback,
    ZDP_MATCH_DESCRIPTORS_RESPONSE : rsp_zdp_match_descriptors_response_callback,
    ZDP_DEVICE_ANNOUNCE_RESPONSE   : rsp_zdp_device_announce_response_callback,
}

RSP_CALLBACKS = {}

''' DO NOT MODIFIED ZCL_CALLBACKS '''
ZCL_CALLBACKS = {
    ZCL_FRAMETYPE_PROFILE_WIDE     : ZCL_PROFILE_WIDE_COMMANDS,
    ZCL_FRAMETYPE_CLUSTER_SPECIFIC : ZCL_CLUSTER_SPECIFIC_COMMANDS,
}
