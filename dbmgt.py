# -*- coding: utf-8 -*-

import sqlite3
import typedef
import traceback
import time
import sys
import binascii

dbConn = None


def init_data_store(dbName):
    global dbConn
    try:
        dbConn = sqlite3.connect(dbName)
        cur = dbConn.cursor()
        try:
            cur.execute(typedef.CREATE_RSP_RECORD_TABLE)
            dbConn.commit()
        except sqlite3.OperationalError:
            pass

    except Exception:
        traceback.print_exc()
        sys.exit(1)


def add_rsp_record(rsp):
    global dbConn
    cur = dbConn.cursor()
    cur.execute(typedef.INSERT_RSP_RECORD,
        (
            int(time.time()),
            rsp['type'],
            rsp['sequence'],
            rsp['euinwk'],
            rsp['rssi'],
            rsp['lqi'],
            rsp['profileid'],
            rsp['clusterid'],
            rsp['srcep'],
            rsp['destep'],
            rsp['cmdid'],
            rsp['frametype'],
            rsp['direction'],
            rsp['mfgid'],
            binascii.hexlify(rsp['apppayload'])
        )
    )
    dbConn.commit()


def query_rsp_record(whereCond):
    global dbConn

    SQLCmd = ' SELECT * FROM rsp_record WHERE 1 '

    if whereCond['type'] is not None:
        SQLCmd += ' AND type=%s ' % (whereCond['type'])

    if whereCond['euinwk'] is not None:
        SQLCmd += ' AND euinwk=%s ' % (whereCond['euinwk'])

    if whereCond['clusterid'] is not None:
        SQLCmd += ' AND clusterid=%d ' % (whereCond['clusterid'])

    if whereCond['profileid'] is not None:
        SQLCmd += ' AND profileid=%d ' % (whereCond['profileid'])

    if whereCond['srcep'] is not None:
        SQLCmd += ' AND srcep=%d ' % (whereCond['srcep'])

    if whereCond['destep'] is not None:
        SQLCmd += ' AND destep=%d ' % (whereCond['destep'])

    if whereCond['cmdid'] is not None:
        SQLCmd += ' AND cmdid=%d ' % (whereCond['cmdid'])

    if whereCond['frametype'] is not None:
        SQLCmd += ' AND frametype=%d ' % (whereCond['frametype'])

    if whereCond['direction'] is not None:
        SQLCmd += ' AND direction=%d ' % (whereCond['direction'])

    if whereCond['mfgid'] is not None:
        SQLCmd += ' AND mfgid=%d ' % (whereCond['mfgid'])

    if whereCond['mintime'] is not None and whereCond['maxtime'] is not None:
        SQLCmd += ' AND rectime BETWEEN %lld AND %lld ' % (whereCond['mintime'], whereCond['maxtime'])

    cur = dbConn.cursor()
    print('SQL=%s\n', SQLCmd)
    cur.execute(SQLCmd)
    result = cur.fetchall()
    return result


def get_ZoneStatusChangeNotification_record(euinwk=None, mintime=0, maxtime=0):
    whereCond = {}
    if euinwk is not None:
        whereCond['euinwk'] = euinwk

    if mintime != 0 and maxtime != 0:
        whereCond['mintime'] = mintime
        whereCond['maxtime'] = maxtime

    result = query_rsp_record(whereCond)
    return result
