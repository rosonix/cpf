# -*- coding: utf-8 -*-

'''
Created on Jan 27, 2014

@author: cwsun@rosonix.com
'''


import time
import rsp
import json
import devicemgt
import requests
import traceback
import rsxrpc
from typedef import *
import os


def do_rpccall(rpcstr):
    retRPC = None
    try:
        rpccmd = json.loads(rpcstr)
        rpccall = getattr(rsxrpc, rpccmd['rpc_cmd'])
        retRPC = rpccall(rpccmd)
    except Exception :
        traceback.print_exc()
        retRPC = {
            'rpc_err' : {'err': 0x02, 'description' : 'RSX RPC Command is Not supported'},
            'rpcstr' : rpcstr
        }

    return retRPC


'''
recvData={"rpc_err":{"err":"0","desc":""},"rpc_id":"1","rpc_cmd":"api_init",
"rpc_return":{"token":"dddb026a992ba41af0fcf88dab22ec346127a729",
"acer_account":"coswedemo@gmail.com","acer_password":"coswedemo1234"}}
'''
TOKEN = None


def api_init(RPCCmd):
    global TOKEN
    TOKEN = RPCCmd['rpc_return']['token']
    return 'api_init'


def webcam_take_videoclip(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    times = 6
    if RPCCmd['rpc_param']['times'] is not None:
        times = RPCCmd['rpc_param']['times']

    tm = time.localtime()
    filename = '%d%02d%02d%02d%02d%02d' % (tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec)
    err = 0
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)

    os.popen('/usr/bin/streamer -c /dev/video0 -t 0:0:%d -frgb24 -r 5 -o %s.avi' % (times, filename))

    return retRPC


def webcam_take_snapshot(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    tm = time.localtime()
    filename = '%d%02d%02d%02d%02d%02d' % (tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec)
    os.popen('/usr/bin/streamer -c /dev/video0 -o %s.jpeg' % (filename))
    err = 0
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def sys_set_calendar_time(RPCCmd) :
    err = 0x02
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err, 'descripton': 'Not support yet'},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }
    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def do_http_action(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    url = RPCCmd['rpc_param']['url']
    r = requests.get(url)
    err = 0
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err, 'HTTPStatus': r.status_code},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_IAS_WD_StartWarning(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    warningInfo = RPCCmd['rpc_param']['warningInfo']
    warningDuration = RPCCmd['rpc_param']['warningDuration']
    for l in euinwklist:
        rspcmd = '$01,%02X,%s,0104,0502,01,%02X,FFFF,00,01,00,FFFF,%02X%04X' % (seq, l['euinwk'], l['endpoint'], warningInfo, warningDuration)
        rsp.rsp_command(rspcmd)
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }
    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def onoff_onoff(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    onoff = RPCCmd['rpc_param']['command_parameter_list'][0]
    devlist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    for l in devlist:
        rspcmd = '$01,%02X,%s,0104,0006,01,%02d,FFFF,%02X,01,00,FFFF\r\n' % (0x0A, l['euinwk'], l['endpoint'], onoff)
        rsp.rsp_command(rspcmd)
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_ON_OFF_Off(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    for l in euinwklist:
        rspcmd = '$01,%02X,%s,0104,0006,01,%02d,FFFF,00,01,00,FFFF\r\n' % (seq, l['euinwk'], l['endpoint'])
        rsp.rsp_command(rspcmd)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_ON_OFF_On(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    for l in euinwklist:
        rspcmd = '$01,%02X,%s,0104,0006,01,%02d,FFFF,01,01,00,FFFF\r\n' % (seq, l['euinwk'], l['endpoint'])
        rsp.rsp_command(rspcmd)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_ON_OFF_Toggle(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    for l in euinwklist:
        rspcmd = '$01,01,%s,0104,0006,01,%02d,FFFF,02,01,00,FFFF\r\n' % (seq, l['euinwk'], l['endpoint'])
        rsp.rsp_command(rspcmd)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def report_device_list_threadfun(delayTime) :
    time.sleep(delayTime)
    RPCCmd = {
        'rpc_cmd' : 'rsp_device_list',
        'rpc_id'  : 32767,
        'rpc_param' : {}
    }
    retRPC = device_ListDevice(RPCCmd)
    wc = STORE['wsclient']
    wc.send(json.dumps(retRPC))


def rsp_device_list(RPCCmd) :
    return device_ListDevice(RPCCmd)


def device_ListDevice(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    devices = devicemgt.get_devices()
    devicesInfo = []
    for dev in devices:
        devDesc = {
            'inputcluster_list'  : dev.inputclusters,
            'outputcluster_list' : dev.outputclusters,
            'euinwk'             : dev.euinwk,
            'endpoint'           : dev.endpoint,
            'profileId'          : dev.profileId,
            'zoneid'             : dev.zoneid,
            'zonetype'           : dev.zonetype,
        }
        devicesInfo.append(devDesc)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_id'     : RPCCmd['rpc_id'],
        'rpc_return' : {'device_list' : devicesInfo},
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_GROUPS_AddGroup(RPCCmd) :
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    groupid = RSPCmd['rpc_param']['groupId']
    for l in euinwklist:
        rspcmd = '$01,%02X,%s,0104,0004,01,%02X,FFFF,00,01,00,FFFF,%02X%02X'\
                    % (seq, l['euinwk'], l['endpoint'], groupid & 0xFF, (groupid & 0xFF00) >> 8)
        rsp.rsp_command(rspcmd)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {'sequence' : 1},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def zigbee_zcl_ha_GROUPS_RemoveGroup(RPCCmd) :
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwklist = RPCCmd['rpc_param']['euinwk_endpoint_list']
    groupid = RSPCmd['rpc_param']['groupId']
    for l in euinwklist:
        rspcmd = '$01,%02X,%s,0104,0004,01,%02X,FFFF,03,01,00,FFFF,%02X%02X'  \
                    % (seq, l['euinwk'], l['endpoint'], groupid & 0xFF, (groupid & 0xFF00) >> 8)
        rsp.rsp_command(rspcmd)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {'sequence' : 1},
        'rpc_id'     : RPCCmd['rpc_id']
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def rsx_set_smart_home_automation_configuration(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    triggers = RPCCmd['rpc_param']['configuration']
    fp = open(CPF_TST_CFG, 'w')
    fp.write(json.dumps(triggers))
    fp.close()

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {'sequence' : 1},
        'rpc_id'     : RPCCmd['rpc_id'],
    }

    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC


def api_init_request(username='', passwd='', gwid='', gwname=''):
    RPCCmd = {
        'rpc_cmd' : 'api_init',
        'rpc_id' : '1',
        'rpc_param' : {
            'account'      : username,
            'password'     : passwd,
            'gateway_id'   : gwid,
            'gateway_name' : gwname,
        }
    }
    print 'api_init_request %s' % (RPCCmd)
    return RPCCmd


'''
schedule table format
{
    SCHEDULE:
    [
        {
            'description' : 'i am schedule 1',
            'id' : 'g573_1529_0',
            'timetable' : '* * * * *',
            'tasks' :
            [
                {
                    "rpc_cmd":"zigbee_zcl_ha_ON_OFF_On",
                    "rpc_id":1,
                    "rpc_param":
                    {
                        "sequence":1,
                        "euinwk_endpoint_list":
                        [
                            {"euinwk":"000D6F00004DB975", "endpoint":1},
                            {"euinwk":"000D6F00004D1234", "endpoint":1}
                        ],
                        "device_or_group_list":["group1", "device1"],
                        "tag":["tag1", "tag2"],
                    }
                },
                {
                    "rpc_cmd":"zigbee_zcl_ha_ON_OFF_Off",
                    "rpc_id":1,
                    "rpc_param":
                    {
                        "sequence":1,
                        "euinwk_endpoint_list":
                        [
                            {"euinwk":"000D6F00004DB975", "endpoint":1},
                            {"euinwk":"000D6F00004D1234", "endpoint":1}
                        ]
                        "device_or_group_list":["group1", "device1"],
                        "tag":["tag1", "tag2"],
                    }
                }
            ]
        }
    ]
}
'''


def serialize_schedule_config(schTable) :
    try :

        fp = open(CPF_SCHEDULE_CFG, 'w')
        json.dump(schTable, fp)
        fp.close()

        SCHEDULE = schTable['SCHEDULE']
        # config the crontab
        fp  = open(CPF_CRON_TABLE, 'w')
        for cron in SCHEDULE :
            if len(cron['timetable']) > 5 :
                c = '%s python %s/CPFschedule.py %s \n' % (cron['timetable'], CPF_HOME, cron['id'])
                fp.write(c)

        fp.close()

    except Exception :
        traceback.print_exc()


def remove_schedule(schTable, id):
    for cron in schTable['SCHEDULE'][:] :
        print 'cron id = %s' % (cron['id'])
        index = 0
        if id == cron['id'] :
            schTable['SCHEDULE'].pop(index)
        index += 1

    serialize_schedule_config(schTable)


def sys_add_update_schedule(RPCCmd) :
    err = 0
    try:
        fp = open(CPF_SCHEDULE_CFG, 'r')
        schTable = json.load(fp)
        fp.close()
    except Exception :
        traceback.print_exc()
        schTable = {'SCHEDULE' : []}

    timetable   = RPCCmd['rpc_param']['timetable']
    tasks       = RPCCmd['rpc_param']['tasks']
    id          = RPCCmd['rpc_param']['id']
    description = RPCCmd['rpc_param']['description']
    cronjob = {
                'id'          : id,
                'description' : description,
                'timetable'   : '%s' % (timetable),
                'tasks'       : tasks
            }

    # remove the existing one, then renew it
    remove_schedule(schTable, id)

    schTable['SCHEDULE'].append(cronjob)

    serialize_schedule_config(schTable)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id'],
    }

    return retRPC


def sys_remove_schedule(RPCCmd):
    err = 0

    try:
        fp = open(CPF_SCHEDULE_CFG, 'r')
        schTable = json.load(fp)
        fp.close()
    except Exception :
        traceback.print_exc()
        schTable = {'SCHEDULE' : []}

    id = RPCCmd['rpc_param']['id']

    remove_schedule(schTable, id)

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id'],
    }
    return retRPC


def sys_get_schedule(RPCCmd):
    err = 0
    try:
        fp = open(CPF_SCHEDULE_CFG, 'r')
        schTable = json.load(fp)
        fp.close()
    except Exception :
        traceback.print_exc()
        schTable = {'SCHEDULE' : []}

    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {'SCHEDULE': schTable['SCHEDULE']},
        'rpc_id'     : RPCCmd['rpc_id'],
    }
    return retRPC


def device_AllowToJoin(RPCCmd):
    print 'Request =>' + json.dumps(RPCCmd, indent=None)
    err = 0
    seq = RPCCmd['rpc_param']['sequence']
    euinwk = RPCCmd['rpc_param']['euinwk']
    duration = RPCCmd['rpc_param']['duration']
    rspcmd = '$00,%02X,%s,C001,0C00,FF,FF,FFFF,01,%02X,FF,FFFF' % (seq, euinwk, duration)
    rsp.rsp_command(rspcmd)
    retRPC = {
        'rpc_cmd'    : RPCCmd['rpc_cmd'],
        'rpc_err'    : {'err': err},
        'rpc_return' : {},
        'rpc_id'     : RPCCmd['rpc_id']
    }
    print 'Response =>' + json.dumps(retRPC, indent=None)
    return retRPC
